import numpy as np
import Image
from numpy import loadtxt
from library import predict

# network detail
input_size = 400
hidden_size = 25
num_label = 10

# input
img = Image.open("11.jpg").convert("L")
img_np = np.asarray(img)
X = np.reshape(img_np[0:img_np.size], (1, 400))

params = np.loadtxt("params.txt", delimiter="~", unpack=False)
Theta1 = np.reshape(
    params[0:hidden_size * (input_size + 1)], (hidden_size, input_size + 1))
Theta2 = np.reshape(params[hidden_size * (input_size + 1):params.size], (num_label, hidden_size + 1))

print(predict(X, Theta1, Theta2))
