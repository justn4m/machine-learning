import numpy as np
import math

# m = the number of tests
# input_size = Theta1.shape[1] - 1
# hidden_size = Theta2.shape[1] - 1
# num_label = Theta2.shape[0]
# X  m input_size+1

# all the params was defined in main.py


def convertLabel(m, num_label, y_temp):
    y = np.zeros((m, num_label))
    for i in xrange(0, m):
        if y_temp[i] == 10:
            y[i, 0] = 1
        else:
            y[i, y_temp[i]] = 1
    return y


def randInitializeWeights(size1, size2, eps):
    return np.random.rand(size1, size2) * 2 * eps - eps


def sigmoid(z):
    # activation function
    return 1.0 / (1.0 + np.exp(-z))


def sigmoidGrad(z):
    s = sigmoid(z)
    return s * (1.0 - s)


def feedForward(X, Theta1, Theta2):
    # append a '1' column to the left of X
    # try:
        m = X.shape[0]
        X = np.hstack((np.ones((m, 1)), X))  # X = m input_size+1
        z1 = np.dot(X, Theta1.T)
        a1 = sigmoid(z1)
        a1 = np.hstack((np.ones((m, 1)), a1))  # a1 = m hidden_size+1
        z2 = np.dot(a1, Theta2.T)
        h = sigmoid(z2)                       # h = m num_label
        return h
    # except Exception:
    #     import pdb
    #     pdb.set_trace()


def predict(X, Theta1, Theta2):
    h = feedForward(X, Theta1, Theta2)  # m num_label
    m = X.shape[0]
    num_label = Theta2.shape[0]
    p = np.zeros(m)
    for i in xrange(0, m):
        mx = -100
        imx = 0
        j = 0
        for j in xrange(0, num_label):
            if(h[i, j] > mx):
                mx = h[i, j]
                imx = j
        p[i] = imx
    return p


def costFunction(params, input_size, hidden_size, num_label, X, y, lamb):
    # get m and transpose y
    m = y.shape[0]

    # transform the paramaters
    Theta1 = np.reshape(
        params[0:hidden_size * (input_size + 1)], (hidden_size, input_size + 1))
    Theta2 = np.reshape(
        params[hidden_size * (input_size + 1):params.size], (num_label, hidden_size + 1))
    h = feedForward(X, Theta1, Theta2)

    J = (-1.0 / m) * np.sum(y * np.log(h) + (1.0 - y) * np.log(1.0 - h))
    reg = lamb / (2.0 * m) * (np.sum(Theta1[:, 1:Theta1.shape[1]]
                                     ** 2) + np.sum(Theta2[:, 1:Theta2.shape[1]]**2))
    return J + reg


def gradient(params, input_size, hidden_size, num_label, X, y, lamb):
    # get number of training tests
    m = y.shape[0]

    # # add one ones col to the left
    # X = np.hstack((np.ones((m, 1)), X))  # X num_label input_size+1

    # transform the paramaters
    Theta1 = np.reshape(
        params[0:hidden_size * (input_size + 1)], (hidden_size, input_size + 1))
    Theta2 = np.reshape(
        params[hidden_size * (input_size + 1):params.size], (num_label, hidden_size + 1))

    # form of Theta_grad
    Theta1_grad = np.zeros_like(Theta1)
    Theta2_grad = np.zeros_like(Theta2)
    for i in xrange(0, m):
        # for in each tests
        a1 = np.reshape(X[i, :], (1, input_size))  # 1*input_size
        a1 = np.hstack((np.ones((1, 1)), a1))        # 1*input_size+1
        a2 = sigmoid(np.dot(a1, Theta1.T))      # 1*hidden_size
        a2 = np.hstack((np.ones((1, 1)), a2))   # 1*hidden_size+1
        a3 = sigmoid(np.dot(a2, Theta2.T))      # 1*num_label

        # back propagation
        delta3 = a3 - y[i, :]  # 1*num_label
        delta2 = np.dot(delta3, Theta2[:, 1:hidden_size + 1]) * \
            sigmoidGrad(np.dot(a1, Theta1.T))  # 1*hidden_size

        # plus with big delta
        # import pdb
        # pdb.set_trace()
        Theta1_grad += np.dot(delta2.T, a1)  # hidden_size * input_size + 1
        Theta2_grad += np.dot(delta3.T, a2)  # num_label * hidden_size + 1

    # plus with regulization
    Theta1_grad[:, 1:input_size + 1] += lamb * Theta1[:, 1:input_size + 1]
    Theta2_grad[:, 1:hidden_size + 1] += lamb * Theta2[:, 1:hidden_size + 1]
    Theta1_grad /= m
    Theta2_grad /= m

    grad = np.hstack((Theta1_grad.flatten(), Theta2_grad.flatten()))
    return grad
