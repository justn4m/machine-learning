import scipy.io as spi
import numpy as np
from library import randInitializeWeights, sigmoid, sigmoidGrad, feedForward, costFunction, gradient, randInitializeWeights, convertLabel, predict
from scipy.optimize import fmin_l_bfgs_b
# import library

#======= load data
mat = spi.loadmat('mat/ex4data1.mat')
X = mat['X']
y_temp = mat['y']

# detail of network
lamb = 1
epsilon = 10
maxiter = 800
m = X.shape[0]
input_size = 400
hidden_size = 25
num_label = 10
Theta1 = randInitializeWeights(hidden_size, input_size + 1, 1)
Theta2 = randInitializeWeights(num_label, hidden_size + 1, 1)

# mat = spi.loadmat('mat/ex4weights.mat')
# Theta1 = mat['Theta1']
# Theta2 = mat['Theta2']
# print("Theta1: ", Theta1)
# print("Theta2: ", Theta2)


# convert y to label
y = convertLabel(m, num_label, y_temp)

# flatten Theta
params = np.hstack((Theta1.flatten(), Theta2.flatten()))
grad = gradient(params, input_size, hidden_size, num_label, X, y, lamb)
print grad.shape

# start to run optimize function
result = fmin_l_bfgs_b(costFunction, params, fprime=gradient, args=(
    input_size, hidden_size, num_label, X, y, lamb), approx_grad=False, disp=1)
Theta1 = np.reshape(
    result[0][0:hidden_size * (input_size + 1)], (hidden_size, input_size + 1))
Theta2 = np.reshape(
    result[0][hidden_size * (input_size + 1):result[0].size], (num_label, hidden_size + 1))

f = open('result1.txt', 'w')
f.write(Theta1)
f.write(Theta2)

p = predict(X, Theta1, Theta2)
print p
f.close()
